package com.easy.tickets.payloads;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RouteUpdateRequest {

	private String name;
	private String start;
	private String stop;
	
}