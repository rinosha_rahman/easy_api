package com.easy.tickets.payloads;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusUpdateRequest {

	private String name;
	private String route;
	private BigDecimal minimum;
	private BigDecimal unit;
	
}