package com.easy.tickets.payloads;

import java.math.BigDecimal;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionSaveRequest {

	private String user;
	private String type;
	private String amount;
	private String start;
	private String stop;
	private String count;
	private String unit;
	private String status;
		// TODO Auto-generated method stub
	}
