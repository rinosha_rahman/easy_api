package com.easy.tickets.payloads;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StopUpdateRequest {

	private String name;
	private String route;
	private BigDecimal price;
	private Long count;
	private String location;
}
