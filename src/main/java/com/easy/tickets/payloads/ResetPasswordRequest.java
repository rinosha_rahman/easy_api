package com.easy.tickets.payloads;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResetPasswordRequest {

	private String username;

	private String token;

	private String password;

	private String confirmpassword;
}