package com.easy.tickets.payloads;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateRequest {

	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String addressLine1;
	private String addressLine2;
	private String city;
}