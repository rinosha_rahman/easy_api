package com.easy.tickets.payloads;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignUpRequest {

	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String addressLine1;
	private String addressLine2;
	private String city;
}