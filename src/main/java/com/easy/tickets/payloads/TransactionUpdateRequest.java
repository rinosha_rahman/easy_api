package com.easy.tickets.payloads;

import java.math.BigDecimal;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionUpdateRequest {

	private String user;
	private String type;
	private BigDecimal amount;
	private String start;
	private String stop;
	private Long count;
	private BigDecimal unit;
	private String status;
		// TODO Auto-generated method stub
	}
