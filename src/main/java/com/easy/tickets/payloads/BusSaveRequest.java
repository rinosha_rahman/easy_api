package com.easy.tickets.payloads;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusSaveRequest {

	private String name;
	private String route;
	private String minimum;
	private String unit;
	
}