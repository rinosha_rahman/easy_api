package com.easy.tickets.repository;



import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


import com.easy.tickets.models.Transaction;
import com.easy.tickets.models.User;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Optional<Transaction>findByUserAndEnabled(User user, boolean b);

	Transaction findById(long id);


}
