package com.easy.tickets.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easy.tickets.models.Route;

public interface RouteRepository extends JpaRepository<Route, Long> {

	boolean existsByName(String name);

	Route findByName(String name);
    Optional<Route>findByNameAndEnabled(String name, boolean b);

	Route findById(long id);
}