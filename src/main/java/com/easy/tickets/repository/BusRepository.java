package com.easy.tickets.repository;

import java.util.Optional;




//import java.time.Instant;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easy.tickets.models.Bus;




public interface BusRepository extends JpaRepository<Bus, Long>
{

boolean existsByName(String name);

	Optional<Bus> findByName(String username);
    Optional<Bus>findByNameAndEnabled(String name, boolean b);


	Bus findById(long id);
}