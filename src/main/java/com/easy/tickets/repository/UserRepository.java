package com.easy.tickets.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.easy.tickets.models.Role;
import com.easy.tickets.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String username);
	Boolean existsByUsername(String username);
	Optional<User> findByUsernameAndEnabled(String name, boolean b);
	Page<User> findByRolesAndEnabled(Role role, boolean b, Pageable pageable);
	User findById(long id);
}