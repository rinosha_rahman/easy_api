package com.easy.tickets.repository;




import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easy.tickets.models.Stop;

public interface StopRepository extends JpaRepository<Stop, Long> {

	boolean existsByName(String name);

	Optional<Stop> findByName(String name);
    Optional<Stop>findByNameAndEnabled(String name, boolean b);

	Stop findById(long id);
}