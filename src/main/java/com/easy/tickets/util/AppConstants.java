package com.easy.tickets.util;

public interface AppConstants {
	String DEFAULT_PAGE_NUMBER = "0";
	String DEFAULT_PAGE_SIZE = "1000";

	int MAX_PAGE_SIZE = 50;
}