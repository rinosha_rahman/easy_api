package com.easy.tickets.security;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.easy.tickets.models.User;

public class RoutePrincipal implements UserDetails {

	private static final long serialVersionUID = -4071262106977109634L;

	private Long id;

	private String username;

	private String email;

	private String phone;

	@JsonIgnore
	private String password;

	@JsonIgnore
	private boolean enabled;

	@JsonIgnore
	private boolean locked;

	private Collection<? extends GrantedAuthority> authorities;

	public RoutePrincipal(Long id, String username, String email, String phone, String password, boolean enabled,
			boolean locked, Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.phone = phone;
		this.password = password;
		this.enabled = enabled;
		this.locked = locked;

		this.authorities = authorities;
	}

	public static RoutePrincipal create(User user) {
		List<GrantedAuthority> authorities = user.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());

		return new RoutePrincipal(user.getId(), user.getUsername(), user.getEmail(), user.getPhone(), user.getPassword(),
				user.isEnabled(), user.isLocked(), authorities);
	}

	public Long getId() {
		return id;
	}

	public String getUserName() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	@Override
	public String getUsername() {
		return email;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !locked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		RoutePrincipal that = (RoutePrincipal) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id);
	}
}