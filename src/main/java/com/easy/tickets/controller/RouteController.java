package com.easy.tickets.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import com.easy.tickets.models.Route;

import com.easy.tickets.payloads.ApiResponse;
import com.easy.tickets.payloads.PagedResponse;
import com.easy.tickets.payloads.RouteSaveRequest;
import com.easy.tickets.payloads.RouteUpdateRequest;
import com.easy.tickets.repository.RouteRepository;

import com.easy.tickets.security.CurrentBus;
import com.easy.tickets.security.RoutePrincipal;
import com.easy.tickets.services.RouteService;
import com.easy.tickets.util.AppConstants;

@RestController
@RequestMapping("/api/route")
public class RouteController {

	@Autowired
	RouteRepository routeRepository;

	@PostMapping("/save")
	
	
	public ResponseEntity<?> routeSave(@Valid @RequestBody RouteSaveRequest routeSaveRequest) {

		if (routeRepository.existsByName(routeSaveRequest.getName())) {
			return new ResponseEntity<>(new ApiResponse(false, "RouteName already  exists"), HttpStatus.BAD_REQUEST);
		}
		Route route = new Route();
		route.setName(routeSaveRequest.getName());
		route.setStart(routeSaveRequest.getStart());
		route.setStop(routeSaveRequest.getStop());
		routeRepository.save(route);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{id}")
				.buildAndExpand(route.getId()).toUri();
		return ResponseEntity.created(location).body(new ApiResponse(true, " Route added successfully", route));	
		
		
}
	@PostMapping("/update")

	public ResponseEntity<?> userUpdate(@Valid @RequestBody RouteUpdateRequest routeUpdateRequest) {
		
		Route route = new Route();
		route.setName(routeUpdateRequest.getName());
		route.setStart(routeUpdateRequest.getStart());
		route.setStop(routeUpdateRequest.getStop());
		route.setEnabled(true);
		route.setLocked(false);
		
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{id}")
				.buildAndExpand(route.getId()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully", route));
	}
	
	@GetMapping("/list")
	public ResponseEntity <?> listMembers(@CurrentBus RoutePrincipal currentRoute,
		@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
		@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

	PagedResponse<Route> routes = RouteService.getMembers(currentRoute, page, size);


	return ResponseEntity.ok(routes);

	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> memberDetails(@PathVariable(name="id") Long id){
	Route route = routeRepository.findById(1);
	return ResponseEntity.ok(route);
	}
//	@DeleteMapping("/{id}")
//	public ResponseEntity<?> memberDelete(@PathVariable(name="id" )Long id) {
//	Route route  = routeRepository.findById(1);
//	route.setEnabled(false);	
//	return ResponseEntity.ok(route);
//	}

}