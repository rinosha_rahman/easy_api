
package com.easy.tickets.controller;

import java.net.URI;
import java.util.Collections;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.easy.tickets.exception.AppException;
import com.easy.tickets.models.Role;
import com.easy.tickets.models.RoleName;
import com.easy.tickets.models.User;
import com.easy.tickets.payloads.ApiResponse;
import com.easy.tickets.payloads.PagedResponse;
import com.easy.tickets.payloads.SignUpRequest;
import com.easy.tickets.payloads.UpdateRequest;
import com.easy.tickets.repository.RoleRepository;
import com.easy.tickets.repository.UserRepository;
import com.easy.tickets.security.CurrentUser;
import com.easy.tickets.security.UserPrincipal;
import com.easy.tickets.services.UserService;
import com.easy.tickets.util.AppConstants;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserService userService;

	@PostMapping("/save")
	public ResponseEntity<?> userSave(@Valid @RequestBody SignUpRequest signUpRequest) {

		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ApiResponse(false, "Username already in use"), HttpStatus.BAD_REQUEST);
		}

		User user = new User();
		user.setUsername(signUpRequest.getUsername());
		user.setPassword(signUpRequest.getPassword());
		user.setFirstName(signUpRequest.getFirstName());
		user.setLastName(signUpRequest.getLastName());
		user.setEmail(signUpRequest.getEmail());
		user.setPhone(signUpRequest.getPhone());
		user.setAddressLine1(signUpRequest.getAddressLine1());
		user.setAddressLine2(signUpRequest.getAddressLine2());
		user.setCity(signUpRequest.getCity());
		user.setFcmId("");
		user.setEnabled(true);
		user.setLocked(false);
		user.setPassword(passwordEncoder.encode(user.getPassword()));

		RoleName role = RoleName.ROLE_DRIVER;
		Role userRole = roleRepository.findByName(role).orElseThrow(() -> new AppException("User Role not set."));

		user.setRoles(Collections.singleton(userRole));

		user = userRepository.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{id}")
				.buildAndExpand(user.getId()).toUri();
		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully", user));
	}

	@PostMapping("/update")
	public ResponseEntity<?> userUpdate(@Valid @RequestBody UpdateRequest updateRequest) {

		Optional<User> user = userRepository.findByUsername(updateRequest.getUsername());
		if (user.isPresent()) {
			
		
		
		user.get().setUsername(updateRequest.getUsername());
		user.get().setFirstName(updateRequest.getFirstName());
		user.get().setLastName(updateRequest.getLastName());
		user.get().setEmail(updateRequest.getEmail());
		user.get().setPhone(updateRequest.getPhone());
		user.get().setAddressLine1(updateRequest.getAddressLine1());
		user.get().setAddressLine2(updateRequest.getAddressLine2());
		user.get().setCity(updateRequest.getCity());
		user.get().setFcmId("");
		user.get().setEnabled(true);

		user.get().setLocked(false);

		 userRepository.save(user.get());

		


		return ResponseEntity.ok(new ApiResponse(true, "User registered successfully", user.get()));
		}else {
			return new ResponseEntity<>(new ApiResponse(false, "username not in use"),HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/list")
	public ResponseEntity<?> listMembers(@CurrentUser UserPrincipal currentUser,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

		PagedResponse<User> users = userService.getMembers(currentUser, page, size);

		return ResponseEntity.ok(users);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> memberDetails(@PathVariable(name="id") Long id){
	User user = userRepository.findById(1);
	return ResponseEntity.ok(user);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> memberDelete(@PathVariable(name="id" )Long id) {
	User user = userRepository.findById(1);
	user.setEnabled(false);	
	userRepository.save(user);
	return ResponseEntity.ok(user);
 
	
	

	}
}

















