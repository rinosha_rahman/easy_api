
package com.easy.tickets.controller;

import java.net.URI;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.easy.tickets.exception.AppException;
import com.easy.tickets.exception.BadRequestException;
import com.easy.tickets.models.JwtRefreshToken;
import com.easy.tickets.models.PasswordResetToken;
import com.easy.tickets.models.RecordStatus;
import com.easy.tickets.models.Role;
import com.easy.tickets.models.RoleName;
import com.easy.tickets.models.User;
import com.easy.tickets.payloads.ApiResponse;
import com.easy.tickets.payloads.JwtAuthenticationResponse;
import com.easy.tickets.payloads.LoginRequest;
import com.easy.tickets.payloads.RefreshTokenRequest;
import com.easy.tickets.payloads.ResetPasswordRequest;
import com.easy.tickets.payloads.SignUpRequest;
import com.easy.tickets.repository.JwtRefreshTokenRepository;
import com.easy.tickets.repository.PasswordResetTokenRepository;
import com.easy.tickets.repository.RoleRepository;
import com.easy.tickets.repository.UserRepository;
import com.easy.tickets.security.JwtTokenProvider;
import com.easy.tickets.security.UserPrincipal;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordResetTokenRepository passwordResetTokenRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtTokenProvider tokenProvider;

	@Autowired
	JwtRefreshTokenRepository jwtRefreshTokenRepository;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);

		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

		String jwt = tokenProvider.generateToken(userPrincipal);
		String refreshToken = tokenProvider.generateRefreshToken();

		saveRefreshToken(userPrincipal, refreshToken);

		if (!(loginRequest.getFcmId() == null || loginRequest.getFcmId().equals(""))) {
			Optional<User> user = userRepository.findByUsernameAndEnabled(loginRequest.getUsername(), true);
			if (!user.isPresent())
				return new ResponseEntity<>(new ApiResponse(false, "Invalid Username!"), HttpStatus.BAD_REQUEST);

			user.get().setFcmId(loginRequest.getFcmId());
			userRepository.save(user.get());
		}

		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, refreshToken, userPrincipal));
	}

	@PostMapping("/refreshToken")
	public ResponseEntity<?> refreshAccessToken(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
		return jwtRefreshTokenRepository.findById(refreshTokenRequest.getRefreshToken()).map(jwtRefreshToken -> {
			User user = jwtRefreshToken.getUser();
			UserPrincipal userPrincipal = UserPrincipal.create(user);
			String accessToken = tokenProvider.generateToken(UserPrincipal.create(user));
			return ResponseEntity
					.ok(new JwtAuthenticationResponse(accessToken, jwtRefreshToken.getToken(), userPrincipal));
		}).orElseThrow(() -> new BadRequestException("Invalid Refresh Token"));
	}

	private void saveRefreshToken(UserPrincipal userPrincipal, String refreshToken) {
		// Persist Refresh Token

		JwtRefreshToken jwtRefreshToken = new JwtRefreshToken(refreshToken);
		jwtRefreshToken.setUser(userRepository.getOne(userPrincipal.getId()));

		Instant expirationDateTime = Instant.now().plus(360, ChronoUnit.DAYS); // Todo Add this in
																				// application.properties
		jwtRefreshToken.setExpirationDateTime(expirationDateTime);

		jwtRefreshTokenRepository.save(jwtRefreshToken);
	}

	@PostMapping("/register")
	public ResponseEntity<?> registerCloudUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ApiResponse(false, "Username already in use"), HttpStatus.BAD_REQUEST);
		}

		User user = new User();
		user.setUsername(signUpRequest.getUsername());
		user.setPassword(signUpRequest.getPassword());
		user.setFirstName(signUpRequest.getFirstName());
		user.setLastName(signUpRequest.getLastName());
		user.setEmail(signUpRequest.getEmail());
		user.setPhone(signUpRequest.getPhone());
		user.setAddressLine1(signUpRequest.getAddressLine1());
		user.setAddressLine2(signUpRequest.getAddressLine2());
		user.setCity(signUpRequest.getCity());
		user.setFcmId("");
		user.setEnabled(true);
		user.setLocked(false);
		user.setPassword(passwordEncoder.encode(user.getPassword()));

		RoleName role = RoleName.ROLE_PASSENGER;
		Role userRole = roleRepository.findByName(role).orElseThrow(() -> new AppException("User Role not set."));

		user.setRoles(Collections.singleton(userRole));

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{id}")
				.buildAndExpand(user.getId()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully", user));

	}

	@GetMapping("/forgotpassword")
	public ResponseEntity<?> forgotPassword(@RequestParam(value = "username") String username) {
		Optional<User> user = userRepository.findByUsernameAndEnabled(username, true);
		if (!user.isPresent())
			return new ResponseEntity<>(new ApiResponse(false, "Invalid Username!"), HttpStatus.BAD_REQUEST);

		String token = UUID.randomUUID().toString();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, 2);

		PasswordResetToken passwordResetToken = new PasswordResetToken();
		passwordResetToken.setUser(user.get());
		passwordResetToken.setToken(token);
		passwordResetToken.setExpiryDate(cal.getTime());
		passwordResetTokenRepository.save(passwordResetToken);

		String message = "<a href='http://localhost:4200/resetpassword?userid=" + user.get().getId() + "&token=" + token
				+ ">Click Here</a>"; // a link for reset password , also send with mail

		return ResponseEntity.ok(new ApiResponse(true, "Forgot Password Email sent successfully"));
	}

	@GetMapping("/validateToken")
	public ResponseEntity<?> showChangePasswordPage(@RequestParam("userid") long userid,
			@RequestParam("token") String token) {

		Optional<PasswordResetToken> passToken = passwordResetTokenRepository.findByUserIdAndToken(userid, token);
		if (!passToken.isPresent()) {
			return new ResponseEntity<>(new ApiResponse(false, "Invalid Token!"), HttpStatus.BAD_REQUEST);
		}

		Calendar cal = Calendar.getInstance();
		if ((passToken.get().getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			return new ResponseEntity<>(new ApiResponse(false, "Token Expired!"), HttpStatus.BAD_REQUEST);
		}

		User user = passToken.get().getUser();
		Authentication auth = new UsernamePasswordAuthenticationToken(user, null,
				Arrays.asList(new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE")));
		SecurityContextHolder.getContext().setAuthentication(auth);
		return ResponseEntity.ok(new ApiResponse(true, "Password Validated successfully" + auth));

	}

	@PostMapping("/resetpassword")
	public ResponseEntity<?> savePassword(@Valid @RequestBody ResetPasswordRequest resetPasswordRequest) {

		Optional<User> user = userRepository.findByUsernameAndEnabled(resetPasswordRequest.getUsername(), true);
		if (!user.isPresent())
			return new ResponseEntity<>(new ApiResponse(false, "Invalid Username!"), HttpStatus.BAD_REQUEST);

		Optional<PasswordResetToken> passToken = passwordResetTokenRepository.findByUserIdAndToken(user.get().getId(),
				resetPasswordRequest.getToken());
		if (!passToken.isPresent()) {
			return new ResponseEntity<>(new ApiResponse(false, "Invalid Token!"), HttpStatus.BAD_REQUEST);
		}

		Calendar cal = Calendar.getInstance();
		if ((passToken.get().getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			return new ResponseEntity<>(new ApiResponse(false, "Token Expired!"), HttpStatus.BAD_REQUEST);
		}

		if (!resetPasswordRequest.getPassword().equals(resetPasswordRequest.getConfirmpassword()))
			return new ResponseEntity<>(new ApiResponse(false, "Password doesn't match!"), HttpStatus.BAD_REQUEST);

		user.get().setPassword(passwordEncoder.encode(resetPasswordRequest.getPassword()));
		userRepository.save(user.get());

		return ResponseEntity.ok(new ApiResponse(true,
				"Reset password successfully" + SecurityContextHolder.getContext().getAuthentication().getPrincipal()));

	}

}