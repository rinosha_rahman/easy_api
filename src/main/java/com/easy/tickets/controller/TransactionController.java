package com.easy.tickets.controller;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.easy.tickets.models.Stop;
import com.easy.tickets.models.Transaction;
import com.easy.tickets.models.User;
import com.easy.tickets.payloads.ApiResponse;
import com.easy.tickets.payloads.PagedResponse;
import com.easy.tickets.payloads.TransactionSaveRequest;
import com.easy.tickets.payloads.TransactionUpdateRequest;
import com.easy.tickets.repository.StopRepository;
import com.easy.tickets.repository.TransactionRepository;
import com.easy.tickets.repository.UserRepository;
import com.easy.tickets.security.CurrentBus;
import com.easy.tickets.security.TransactionPrincipal;
import com.easy.tickets.services.TransactionService;
import com.easy.tickets.util.AppConstants;

@RestController
@RequestMapping("/api/transaction")

public class TransactionController {

	@Autowired
	StopRepository stopRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@PostMapping("/save")

	public ResponseEntity<?> transacationSave(@Valid @RequestBody TransactionSaveRequest transactionSaveRequest) {

		Optional<User> user = userRepository.findByUsername(transactionSaveRequest.getUser());
		Optional<Stop> start = stopRepository.findByName(transactionSaveRequest.getStart());
		Optional<Stop> stop = stopRepository.findByName(transactionSaveRequest.getStop());


		if (!user.isPresent())
			return new ResponseEntity<>(new ApiResponse(false, "User not available"), HttpStatus.BAD_REQUEST);

		if (!start.isPresent())
			return new ResponseEntity<>(new ApiResponse(false, "Start location not available"), HttpStatus.BAD_REQUEST);

		if (!stop.isPresent())
			return new ResponseEntity<>(new ApiResponse(false, "Start location not available"), HttpStatus.BAD_REQUEST);

		Transaction transaction = new Transaction();

		transaction.setUser(user.get().getId());
		transaction.setType(transactionSaveRequest.getType());
		transaction.setAmount(new BigDecimal(transactionSaveRequest.getAmount()));
		transaction.setCount(Long.parseLong(transactionSaveRequest.getCount()));
		transaction.setUnit(new BigDecimal(transactionSaveRequest.getUnit()));
		transaction.setStatus(transactionSaveRequest.getStatus());
		transaction.setStart(start.get().getId());
		transaction.setStop(stop.get().getId());

		transactionRepository.save(transaction);

		return ResponseEntity.ok(new ApiResponse(true, "Transaction successfull", transaction));

	}

	@PostMapping("/update")
	public ResponseEntity<?> transationUpdate(@Valid @RequestBody TransactionUpdateRequest transactionUpdateRequest) {

		Optional<User> user = userRepository.findByUsername(transactionUpdateRequest.getUser());
		Optional<Stop> start = stopRepository.findByName(transactionUpdateRequest.getStart());
		Optional<Stop> stop = stopRepository.findByName(transactionUpdateRequest.getStop());

		if (!start.isPresent())
			return new ResponseEntity<>(new ApiResponse(false, "Start location not available"), HttpStatus.BAD_REQUEST);

		if (!stop.isPresent())
			return new ResponseEntity<>(new ApiResponse(false, "Start location not available"), HttpStatus.BAD_REQUEST);

		if (user.isPresent()) {
			Transaction transaction = new Transaction();

			transaction.setUser(user.get().getId());
			transaction.setType(transactionUpdateRequest.getType());
			transaction.setAmount(transactionUpdateRequest.getAmount());
			transaction.setCount(transactionUpdateRequest.getCount());
			transaction.setUnit(transactionUpdateRequest.getUnit());
			transaction.setStatus(transactionUpdateRequest.getStatus());
			transaction.setStart(start.get().getId());
			transaction.setStop(stop.get().getId());

			URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/")
					.buildAndExpand(transaction.getId()).toUri();

			return ResponseEntity.created(location)
					.body(new ApiResponse(true, "Bus registered successfully", transaction));
		} else {
			return new ResponseEntity<>(new ApiResponse(false, "transaction not possible"), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/list")
	public ResponseEntity<?> listMembers(@CurrentBus TransactionPrincipal currentTransaction,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

		PagedResponse<Transaction> transactions = TransactionService.getMembers(currentTransaction, page, size);

		return ResponseEntity.ok(transactions);

	}

	@GetMapping("/{id}")
	public ResponseEntity<?> memberDetails(@PathVariable(name = "id") Long id) {
		Transaction transaction = transactionRepository.findById(1);
		return ResponseEntity.ok(transaction);
	}


	@DeleteMapping("/{id}")
	public ResponseEntity<?> memberDelete(@PathVariable(name = "id") Long id) {
		Transaction transaction = transactionRepository.findById(1);
		transaction.setEnabled(false);
		return ResponseEntity.ok(transaction);
	}
}