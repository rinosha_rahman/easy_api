package com.easy.tickets.controller;

import java.math.BigDecimal;
import java.net.URI;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.easy.tickets.models.Bus;
import com.easy.tickets.models.Route;

import com.easy.tickets.payloads.ApiResponse;
import com.easy.tickets.payloads.BusSaveRequest;
import com.easy.tickets.payloads.BusUpdateRequest;
import com.easy.tickets.payloads.PagedResponse;
import com.easy.tickets.repository.BusRepository;
import com.easy.tickets.repository.RouteRepository;
import com.easy.tickets.security.BusPrincipal;
import com.easy.tickets.security.CurrentBus;
import com.easy.tickets.services.BusService;
import com.easy.tickets.util.AppConstants;

@RestController
@RequestMapping("/api/bus")
public class BusController {

	@Autowired
	BusRepository busRepository;

	@Autowired
	RouteRepository routeRepository;
	@Autowired
	BusService busService;

	@PostMapping("/save")
	public ResponseEntity<?> busSave(@Valid @RequestBody BusSaveRequest busSaveRequest) {

		if (busRepository.existsByName(busSaveRequest.getName())) {
			return new ResponseEntity<>(new ApiResponse(false, "Bus name already in use"), HttpStatus.BAD_REQUEST);
		}
		if (!routeRepository.existsByName(busSaveRequest.getRoute())) {
			return new ResponseEntity<>(new ApiResponse(false, "Route unavailable"), HttpStatus.BAD_REQUEST);

		}

		Route route = routeRepository.findByName(busSaveRequest.getRoute());

		Bus bus = new Bus();
		bus.setName(busSaveRequest.getName());
		bus.setRoute(route);
		bus.setUnit(new BigDecimal(busSaveRequest.getUnit()));
		bus.setMinimum(new BigDecimal(busSaveRequest.getMinimum()));
		bus.setEnabled(true);
		bus.setLocked(false);
		busRepository.save(bus);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{id}")
				.buildAndExpand(bus.getId()).toUri();
		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully", bus));
	}

	@PostMapping("/update")
	public ResponseEntity<?> busUpdate(@Valid @RequestBody BusUpdateRequest busUpdateRequest) {

		if (!routeRepository.existsByName(busUpdateRequest.getRoute())) {
			return new ResponseEntity<>(new ApiResponse(false, "Bus not registered"), HttpStatus.BAD_REQUEST);

		}

		Route route = routeRepository.findByName(busUpdateRequest.getRoute());
		Bus bus = busRepository.findByName(busUpdateRequest.getName()).get();
		bus.setName(busUpdateRequest.getName());
		bus.setRoute(route);
		bus.setUnit(busUpdateRequest.getUnit());
		bus.setMinimum(busUpdateRequest.getMinimum());
		bus.setEnabled(true);
		bus.setLocked(false);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/").buildAndExpand(bus.getId())
				.toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "Bus registered successfully", bus));
	}

	@GetMapping("/list")
	public ResponseEntity<?> listMembers(@CurrentBus BusPrincipal currentBus,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

		PagedResponse<Bus> bus = busService.getMembers(currentBus, page, size);
		return ResponseEntity.ok(bus);

	}

	@GetMapping("/{id}")
	public ResponseEntity<?> memberDetails(@PathVariable(name = "id") Long id) {
		Bus bus = busRepository.findById(1);
		return ResponseEntity.ok(bus);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> memberDelete(@PathVariable(name = "id") Long id) {
		Bus bus = busRepository.findById(1);
		bus.setEnabled(false);
		busRepository.save(bus);
		return ResponseEntity.ok(bus);

	}


}
