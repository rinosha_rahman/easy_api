
package com.easy.tickets.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.easy.tickets.models.RecordStatus;
import com.easy.tickets.payloads.ApiResponse;
import com.easy.tickets.repository.UserRepository;
import com.easy.tickets.security.CurrentUser;
import com.easy.tickets.security.UserPrincipal;

@RestController
@RequestMapping("/api/demo")
public class ExploreController {

	@Autowired
	UserRepository userRepository;

	@GetMapping
	public String getDemo() {
		return "Its working!";
	}

	@GetMapping("/test")
	public String listDemoByTest() {
		return "Test is working";
	}

	@GetMapping("/{variable}")
	public String listDemoByVariable(@PathVariable("variable") String variable) {
		return "Path variable is working & is " + variable;
	}
}
