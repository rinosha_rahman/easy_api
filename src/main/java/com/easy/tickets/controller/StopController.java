package com.easy.tickets.controller;

import java.net.URI;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.easy.tickets.models.Route;
import com.easy.tickets.models.Stop;

import com.easy.tickets.payloads.ApiResponse;
import com.easy.tickets.payloads.PagedResponse;
import com.easy.tickets.payloads.StopSaveRequest;
import com.easy.tickets.payloads.StopUpdateRequest;
import com.easy.tickets.repository.RouteRepository;
import com.easy.tickets.repository.StopRepository;
import com.easy.tickets.security.CurrentBus;

import com.easy.tickets.security.StopPrincipal;

import com.easy.tickets.services.StopService;
import com.easy.tickets.util.AppConstants;



@RestController
@RequestMapping("/api/stop")
public class StopController {

	@Autowired
	StopRepository stopRepository;
	
	@Autowired
	RouteRepository routeRepository;	


	@PostMapping("/save")
	public ResponseEntity<?> stopSave(@Valid @RequestBody StopSaveRequest stopSaveRequest) {

		if (stopRepository.existsByName(stopSaveRequest.getName())) {
			return new ResponseEntity<>(new ApiResponse(false, "StopName already in exists"), HttpStatus.BAD_REQUEST);
		}
		if (!routeRepository.existsByName(stopSaveRequest.getRoute())) {
			return new ResponseEntity<>(new ApiResponse(false, "Route unavailable"), HttpStatus.BAD_REQUEST);

		}
		
		Route route = routeRepository.findByName(stopSaveRequest.getRoute());
		
		Stop stop = new Stop();
		stop.setName(stopSaveRequest.getName());
		stop.setRoute(route);
		stop.setCount(stopSaveRequest.getCount());
		stop.setPrice(stopSaveRequest.getPrice());
		stop.setLocation(stopSaveRequest.getLocation());

		stop.setEnabled(true);
		stop.setLocked(false);
        
		stopRepository.save(stop);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{id}")
				.buildAndExpand(stop.getId()).toUri();
		return ResponseEntity.created(location).body(new ApiResponse(true, " Route added successfully", stop));	
		
		
}
	@PostMapping("/update")
	public ResponseEntity<?> stopUpdate(@Valid @RequestBody StopUpdateRequest stopUpdateRequest) {
		if (!routeRepository.existsByName(stopUpdateRequest.getRoute())) {
			return new ResponseEntity<>(new ApiResponse(false, "transaction not successfull"), HttpStatus.BAD_REQUEST);

		}
		
		Route route = routeRepository.findByName(stopUpdateRequest.getRoute());
		
		Stop stop = new Stop();
		stop.setName(stopUpdateRequest.getName());
		stop.setRoute(route);
		stop.setCount(stopUpdateRequest.getCount());
		stop.setPrice(stopUpdateRequest.getPrice());
		stop.setLocation(stopUpdateRequest.getLocation());

		stop.setEnabled(true);
		stop.setLocked(false);
		
		
		
		
		
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/")
				.buildAndExpand(stop.getId()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "Bus registered successfully", stop));
}
	
	
	@GetMapping("/list")
	public ResponseEntity <?> listMembers(@CurrentBus StopPrincipal currentStop,
		@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
		@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

	PagedResponse<Stop> stops = StopService.getMembers(currentStop, page, size);


	
	return ResponseEntity.ok(stops);

	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> memberDetails(@PathVariable(name="id") Long id){
	Stop stop = stopRepository.findById(1);
	return ResponseEntity.ok(stop);
	}
//	@DeleteMapping("/{id}")
//	public ResponseEntity<?> memberDelete(@PathVariable(name="id" )Long id) {
//	Stop stop  = stopRepository.findById(1);
//	stop.setEnabled(false);	
//	return ResponseEntity.ok(stop);
//	}

}