package com.easy.tickets.models;

import java.math.BigDecimal;



import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


import com.easy.tickets.models.audit.DateAudit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "transaction")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Transaction extends DateAudit {

	private static final long serialVersionUID = -3086703978138901610L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "user", nullable = false)
	private long user;

	@NotBlank
	@Size(max = 40)
	@Column(name = "type", nullable = false)
	private String type;
	
	@Column(name = "amount", nullable = false)
	private BigDecimal amount;
	
	@Column(name = "start", nullable = false)
	private long start;

	@Column(name = "stop", nullable = false)
	private long stop;
	
	@Column(name = "unit", nullable = false)
	private BigDecimal unit;

	@Column(name = "count", nullable = false)
	private Long count;

	@NotBlank
	@Size(max = 40)
	@Column(name = "status", nullable = false)
	private String status;
	
	@Column(name = "enabled")
	private boolean enabled = true;

	@Column(name = "locked")
	private boolean locked = true;

	
}

