package com.easy.tickets.models;

public enum RoleName {
	ROLE_ADMIN, 
	ROLE_DRIVER,
	ROLE_PASSENGER
}