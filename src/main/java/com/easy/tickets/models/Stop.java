package com.easy.tickets.models;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;



import com.easy.tickets.models.audit.DateAudit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "stop")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Stop extends DateAudit {

	private static final long serialVersionUID = -3086703978138901610L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank
	@Size(max = 40)
	@Column(name = "name", nullable = false)
	private String name;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "route", nullable = false)
	private Route route;

	@Column(name = "count", nullable = false)
	private Long count;
	
	@Column(name = "price", nullable = false)
	private BigDecimal price;
	
	@NotBlank
	@Size(max = 40)
	@Column(name = "locatiom", nullable = false)
	private String location;

	@Column(name = "enabled")
	private boolean enabled = true;

	@Column(name = "locked")
	private boolean locked = true;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

	
}

