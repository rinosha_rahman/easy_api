package com.easy.tickets.models;

public enum RecordStatus {
	DELETED, 
	ACTIVE, 
	INACTIVE, 
	APPROVED, 
	DENIED, 
	REQUESTED
}