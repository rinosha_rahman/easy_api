package com.easy.tickets.models;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.easy.tickets.models.audit.DateAudit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "users", uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }) })
@Getter
@Setter
@NoArgsConstructor
@ToString
public class User extends DateAudit {

	private static final long serialVersionUID = -3086703978138901610L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank
	@Size(max = 40)
	@Column(name = "username", nullable = false)
	private String username;

	@JsonIgnore
	@NotBlank
	@Size(max = 250)
	@Column(name = "password", nullable = false)
	private String password;

	@NotBlank
	@Size(max = 40)
	@Column(name = "first_name", nullable = false)
	private String firstName;

	@NotBlank
	@Size(max = 40)
	@Column(name = "last_name", nullable = true)
	private String lastName;

	@Size(max = 40)
	@Email
	@Column(name = "email", nullable = true)
	private String email;

	@Size(max = 15)
	@Column(name = "phone", nullable = true)
	private String phone;

	@Size(max = 20)
	@Column(name = "gender", nullable = true)
	private String gender;

	@Size(max = 250)
	@Column(name = "address_line_1", nullable = true)
	private String addressLine1;
	
	@Size(max = 250)
	@Column(name = "address_line_2", nullable = true)
	private String addressLine2;
	
	@Size(max = 250)
	@Column(name = "city", nullable = true)
	private String city;

	@Size(max = 250)
	@Column(name = "fcm_id", nullable = true)
	private String fcmId;
	
	@Column(name = "wallet", nullable = true, columnDefinition="DECIMAL(10,2) DEFAULT '0'")
	private BigDecimal wallet;

	@Column(name = "enabled")
	private boolean enabled = true;

	@Column(name = "locked")
	private boolean locked = true;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

		// TODO Auto-generated method stub
		
	}

	

	


