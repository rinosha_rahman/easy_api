package com.easy.tickets.services;


import java.util.Collections;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.easy.tickets.models.Role;
import com.easy.tickets.models.RoleName;
import com.easy.tickets.models.User;
import com.easy.tickets.payloads.PagedResponse;
import com.easy.tickets.repository.RoleRepository;
import com.easy.tickets.repository.UserRepository;
import com.easy.tickets.security.UserPrincipal;

@Service
public class UserService {



	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Transactional
	public PagedResponse<User> getMembers(UserPrincipal currentUser, int page, int size) {

		Optional<Role> role = roleRepository.findByName(RoleName.ROLE_DRIVER);

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");

		Page<User> user = userRepository.findByRolesAndEnabled(role.get(), true, pageable);

		if (user.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), user.getNumber(), user.getSize(),
					user.getTotalElements(), user.getTotalPages(), user.isLast());
		}

		return new PagedResponse<>(user.getContent(), user.getNumber(), user.getSize(), user.getTotalElements(),
				user.getTotalPages(), user.isLast());
	}

}
