INSERT INTO `roles` (`id`, `name`)
VALUES
	(1, 'ROLE_ADMIN'),
	(2, 'ROLE_DRIVER'),
	(3, 'ROLE_PASSENGER');
	
INSERT INTO `users` (`id`, `created_at`, `updated_at`, `username`, `password`, `first_name`, `last_name`, `email`, `phone`, `gender`, `address_line_1`,`address_line_2`, `city`, `fcm_id`,`wallet`,`enabled`, `locked`)
VALUES (1, '2018-06-27 13:27:17', '2018-06-27 13:27:17', 'admin', '$2a$10$4Wc7RBqc9GjeSfEga00iDO.BnHA0nwgl9a7Gm3WOqgw5usZe2HtRW', 'Admin', 'User', 'admin@root.com', '9898989898', 'male', ' Address Line 1', 'Address Line 1', ' city', '',0.00, true, false);

INSERT INTO user_roles (user_id, role_id) VALUES (1, 1);	